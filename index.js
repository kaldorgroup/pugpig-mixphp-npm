let mix = require('laravel-mix')

/**
 * PHP processing for Laravel Mix
 */
class Php {
  /**
   * Dependencies for plugin.
   *
   * @return {String[]}
   */
  dependencies() {
    return [
      'copy-webpack-plugin',
    ]
  }

  register(gitversion) {
    this.gitversion = gitversion
  }

  webpackPlugins() {
    let CopyWebpackPlugin = require('copy-webpack-plugin');
    let gitversion = this.gitversion

    return [
      new CopyWebpackPlugin([
        {
          from: 'src/**/*.php',
          transform(content, path) {
            if (gitversion) {
              let original_content_string = content.toString()
              let updated_content_string = original_content_string.replace(/GIT_VERSION/, gitversion())
              if (original_content_string != updated_content_string) {
                return Buffer.from(updated_content_string);
              }
            }
            return content
          },
          transformPath(targetPath, absolutePath) {
            return targetPath.substring(4);
          },
          cache: true,
        },
      ])
    ];
  }
}

mix.extend('php', new Php())